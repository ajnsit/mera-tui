use crossterm::event::Event;

pub trait Update {
    fn update(&mut self, event: Event);
}
