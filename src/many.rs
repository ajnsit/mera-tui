use crossterm::event::{Event, KeyCode, KeyEvent};
use ratatui::buffer::Buffer;
use ratatui::layout::{Constraint, Layout, Rect};
use ratatui::widgets::{StatefulWidgetRef, WidgetRef};

use crate::update::Update;

#[derive(Debug)]
pub struct Many<T> {
    items: Vec<T>,
    selected: usize,
}

impl<T> Many<T> {
    pub fn new(items: Vec<T>) -> Many<T> {
        Many { items, selected: 0 }
    }
}

impl<T: Default + Update> Update for Many<T> {
    fn update(&mut self, event: Event) {
        match event {
            Event::Key(KeyEvent {
                code: KeyCode::Char('j'),
                ..
            }) => {
                if self.selected < self.items.len() - 1 {
                    self.selected += 1;
                } else {
                    self.selected = 0;
                }
            }
            Event::Key(KeyEvent {
                code: KeyCode::Char('k'),
                ..
            }) => {
                if self.selected > 0 {
                    self.selected -= 1;
                } else {
                    self.selected = self.items.len() - 1;
                }
            }
            Event::Key(KeyEvent {
                code: KeyCode::Char('a'),
                ..
            }) => {
                self.items.push(T::default());
            }
            _ => {
                self.items.get_mut(self.selected).map(|c: &mut T| {
                    c.update(event);
                });
            }
        }
    }
}

impl<T: StatefulWidgetRef<State = bool>> WidgetRef for Many<T> {
    fn render_ref(&self, area: Rect, buf: &mut Buffer) {
        let constraints = self.items.iter().map(|_| Constraint::Length(10));
        let l = Layout::vertical(constraints).split(area);
        self.items.iter().enumerate().for_each(|(i, w)| {
            w.render_ref(l[i], buf, &mut (i == self.selected));
        });
    }
}
