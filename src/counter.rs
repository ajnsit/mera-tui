use crossterm::event::{Event, KeyCode, KeyEvent};
use ratatui::{
    buffer::Buffer,
    layout::Rect,
    text::Text,
    widgets::{StatefulWidgetRef, Widget},
};

use crate::update::Update;

#[derive(Debug, Default)]
pub struct Counter {
    value: i64,
}

impl Counter {
    pub fn new(value: i64) -> Counter {
        Counter { value }
    }
}

impl StatefulWidgetRef for Counter {
    type State = bool;
    fn render_ref(&self, area: Rect, buf: &mut Buffer, selected: &mut bool) {
        Text::from(format!(
            "{} Count: {}",
            if *selected { '→' } else { ' ' },
            self.value
        ))
        .render(area, buf);
    }
}

impl Update for Counter {
    fn update(&mut self, event: Event) {
        match event {
            Event::Key(KeyEvent {
                code: KeyCode::Char('+'),
                ..
            }) => {
                self.value += 1;
            }
            Event::Key(KeyEvent {
                code: KeyCode::Char('-'),
                ..
            }) => {
                self.value -= 1;
            }
            _ => {}
        }
    }
}
