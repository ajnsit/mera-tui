use std::io::{self, stdout, Stdout};

use crossterm::{
    event::{self, Event, KeyCode, KeyEventKind},
    execute,
    terminal::*,
};
use ratatui::{prelude::*, widgets::WidgetRef};

use crate::update::Update;

#[derive(Debug)]
pub struct App<T> {
    gui: T,
    exit: bool,
}

impl<T> App<T> {
    pub fn new(gui: T) -> App<T> {
        App { gui, exit: false }
    }
}

impl<T: WidgetRef + Update> App<T> {
    pub fn launch(&mut self) -> io::Result<()> {
        let mut terminal = init()?;
        let app_result = self.run(&mut terminal);
        restore()?;
        app_result
    }

    pub fn run(&mut self, terminal: &mut Tui) -> io::Result<()> {
        loop {
            let App { exit, gui } = self;
            let c: &T = &*gui;
            if *exit {
                break;
            }
            terminal.draw(|frame| {
                frame.render_widget(c, frame.size());
            })?;
            self.handle_events()?;
        }
        Ok(())
    }

    fn handle_events(&mut self) -> io::Result<()> {
        match event::read()? {
            // it's important to check that the event is a key press event as
            // crossterm also emits key release and repeat events on Windows.
            Event::Key(key_event) if key_event.kind == KeyEventKind::Press => {
                if key_event.code == KeyCode::Char('q') {
                    self.exit = true;
                } else {
                    self.gui.update(Event::Key(key_event));
                }
            }
            _ => {}
        };
        Ok(())
    }
}

pub type Tui = Terminal<CrosstermBackend<Stdout>>;

pub fn init() -> io::Result<Tui> {
    execute!(stdout(), EnterAlternateScreen)?;
    enable_raw_mode()?;
    Terminal::new(CrosstermBackend::new(stdout()))
}

pub fn restore() -> io::Result<()> {
    execute!(stdout(), LeaveAlternateScreen)?;
    disable_raw_mode()?;
    Ok(())
}
