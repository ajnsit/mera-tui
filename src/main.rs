use std::io;

use app::App;
use counter::Counter;
use many::Many;

pub mod app;
pub mod counter;
pub mod many;
pub mod update;

fn main() -> io::Result<()> {
    let gui = Many::new(vec![Counter::new(0)]);
    App::new(gui).launch()
}
